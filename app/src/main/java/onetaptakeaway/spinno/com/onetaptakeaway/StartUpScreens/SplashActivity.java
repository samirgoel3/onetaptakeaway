package onetaptakeaway.spinno.com.onetaptakeaway.StartUpScreens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import onetaptakeaway.spinno.com.onetaptakeaway.MainActivity;
import onetaptakeaway.spinno.com.onetaptakeaway.R;

public class SplashActivity extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.abc_fade_in ,R.anim.abc_fade_out);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
