package onetaptakeaway.spinno.com.onetaptakeaway.StartUpScreens;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.LinearLayout;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import butterknife.Bind;
import butterknife.ButterKnife;
import onetaptakeaway.spinno.com.onetaptakeaway.R;
import onetaptakeaway.spinno.com.onetaptakeaway.search.FragmentSearch;

public class MainScreen extends FragmentActivity {


    /////////menu attributes
    MenuDrawer mDrawer ;
    @Bind(R.id.open_menu_in_main_screen)LinearLayout menu_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawer = MenuDrawer.attach(this, Position.LEFT);
        mDrawer.setContentView(R.layout.activity_main_screen);
        mDrawer.setMenuView(R.layout.drawer_menu);
        ButterKnife.bind(this);

///////////////menu button
        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.toggleMenu();
            }
        });


        changefragment(new FragmentSearch() ,"FragmentSearch");

    }

    private void changefragment(FragmentSearch fragment , String fragmentName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.containerin_main_screen , fragment , fragmentName)
                .setTransitionStyle(FragmentTransaction.TRANSIT_ENTER_MASK)
                .commit();
    }
}
