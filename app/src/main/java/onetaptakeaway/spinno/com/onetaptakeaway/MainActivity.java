package onetaptakeaway.spinno.com.onetaptakeaway;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import guillotineMenu.animation.GuillotineAnimation;

public class MainActivity extends Activity {
    private static final long RIPPLE_DURATION = 250;


    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.root) FrameLayout root;
    @Bind(R.id.content_hamburger) View contentHamburger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);





        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        root.addView(guillotineMenu);

        ImageView imbannerview = (ImageView) guillotineMenu.findViewById(R.id.banner_in_menu_background);
                imbannerview.setImageResource(R.drawable.menu_banner_one);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .build().close();

       // banner_image_background.setImageResource(R.drawable.menu_banner_one);
    }
}
